## Hello there :wave:

My name is Sonam Tashi and I currently live in Thimphu, BT.

I've worked at SELISE since 2020 as backend developer mostly

working in ruby on rails apis/services

### How do I work? :computer:


### Meetings? :speech_balloon:


### Knowledge of Software Development :question:

Over my time at SELISE I have gained a large amount of knowledge about

various ways things are done, especially on the backend. This is something

I need to work on giving back others, if you have questions, feel free

to just send them my way.

### Around the web :earth_asia:

I try to remain quiet on the internet :sweat_smile: but if you want

to follow me places:

- [Twitter](https://twitter.com/tashist515)

- [Linkedin](https://www.linkedin.com/in/sonamtaa/)

- [GitHub](https://github.com/sonamtaa)
